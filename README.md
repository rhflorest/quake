Requerimientos:
- Se requiere de java 8
- Repositorio maven instalado en el computador
- Loombok instalado en la maquina https://projectlombok.org/download

Para construir proyecto
- gradlew build

Para corre la aplicación
- gradlew bootrun

Documento swager en:

- http://localhost:8080/swagger-ui.html#/