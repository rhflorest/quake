package com.quake.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.Lists;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;



@Configuration
@EnableSwagger2
public class Swagger2Config {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.quake.controller"))
				.paths(PathSelectors.regex("/.*")).build().apiInfo(apiEndPointsInfo())
				.securitySchemes(Lists.newArrayList(apiKey()));
	}
	private ApiKey apiKey() {
		return new ApiKey("Bearer", "Authorization", "header");
	}
	@Bean
	public SecurityConfiguration security() {
		return new SecurityConfiguration(null, null, null, "my-api", "Bearer", ApiKeyVehicle.HEADER, "Authorization", ",");
	}

	private ApiInfo apiEndPointsInfo() {
		return new ApiInfoBuilder().title("Api quake").description("Api muestra movimientos teluricos")
				.contact(new Contact("Rodrigo Flores", "https://www.intoschool.cl/", "rhflorest@gmail.com")).license("Apache 2.0")
				.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html").version("1.0.0").build();
	}
}
