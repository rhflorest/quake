package com.quake.dto;

import lombok.Data;


/***
 * 
 * @author Rodrigo Flores
 */
@Data
public class FeatureDTO {
	String type;
	PropertiesDTO properties;
	GeometryDTO geometry;
	String id;

}
