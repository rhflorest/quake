package com.quake.dto;

import lombok.Data;

/***
 * 
 * @author Rodrigo Flores
 */
@Data
public class CantidadDTO {
	private Integer count;
	private Integer maxAllowed;
	
	
}
