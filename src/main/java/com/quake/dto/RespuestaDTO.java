package com.quake.dto;

import java.util.List;

import lombok.Data;

/***
 * 
 * @author Rodrigo Flores
 */
@Data
public class RespuestaDTO implements Cloneable {
	String type;
	MetadataDTO metadata;
	List<FeatureDTO> features;
	Double[] bbox;

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
