package com.quake.dto;

import lombok.Data;


/***
 * 
 * @author Rodrigo Flores
 */
@Data
public class MetadataDTO {
	Long generated;
	String  url;
	String title;
	Integer status;
	String api;
	Integer count;

}
