package com.quake.dto;

import lombok.Data;


/***
 * 
 * @author Rodrigo Flores
 */
@Data
public class GeometryDTO {

	String type;
	Double[] coordinates;
}
