package com.quake.dto;

import lombok.Data;


/***
 * 
 * @author Rodrigo Flores
 */
@Data
public class PropertiesDTO {

	Double  mag;
	String place;
	Long tima;
	Long tz;
	String url;
	String detail;
	String felt;
	String cdi;
	String mmi;
	String alert;
	String status;
	Integer tsunami;
	Integer sig;
	String net;
	String code;
	String ids;
	String sources;
	String types;
	Integer nst;
	Double dmin;
	Double rms;
	Integer gap;
	String magType;
	String type;
	String title;
}
