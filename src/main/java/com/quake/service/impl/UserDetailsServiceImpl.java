package com.quake.service.impl;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.quake.dto.UsuarioDTO;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) {
		
		if(!username.trim().toUpperCase().equals("TEST"))
			 throw new UsernameNotFoundException(String.format("Usuario no econtrado '%s'.", username));
		String pass= new BCryptPasswordEncoder().encode("test");
		UserDetails userDetails=new UsuarioDTO("test",pass);
		return userDetails;
	}

}
