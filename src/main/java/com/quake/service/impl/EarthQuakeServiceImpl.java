package com.quake.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.quake.dto.CantidadDTO;
import com.quake.dto.FeatureDTO;
import com.quake.dto.RespuestaDTO;
import com.quake.service.EarthQuakeService;

/***
 * 
 * @author Rodrigo Flores
 */
@Service
public class EarthQuakeServiceImpl implements EarthQuakeService {

	@Value("${urlApi}")
	String urlApi;

	public RespuestaDTO get(LocalDate startTime, LocalDate endTime, Double minMagnitude, Double maxMagnitude) {
		RestTemplate restTemplate = new RestTemplate();
		String url = this.urlApi + "/query?format=geojson";
		if (startTime != null) {
			url = url + "&starttime=" + startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		if (endTime != null) {
			url = url + "&endtime=" + endTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

		}
		if (minMagnitude != null) {
			url = url + "&minmagnitude=" + minMagnitude;
		}
		if (maxMagnitude != null) {
			url = url + "&maxmagnitude=" + maxMagnitude;
		}
		RespuestaDTO resultados = restTemplate.getForObject(url, RespuestaDTO.class);
		return resultados;
	}

	public Integer count(LocalDate startTime, LocalDate endTime, Double minMagnitude, Double maxMagnitude) {
		RestTemplate restTemplate = new RestTemplate();
		String url = this.urlApi + "/count?format=geojson";
		if (startTime != null) {
			url = url + "&starttime=" + startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		if (endTime != null) {
			url = url + "&endtime=" + endTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

		}
		if (minMagnitude != null) {
			url = url + "&minmagnitude=" + minMagnitude;
		}
		if (maxMagnitude != null) {
			url = url + "&maxmagnitude=" + maxMagnitude;
		}

		CantidadDTO cantidad = restTemplate.getForObject(url, CantidadDTO.class);

		return cantidad.getCount();
	}

	@Override
	public RespuestaDTO get(LocalDate fechaInicioR1, LocalDate fechaTerminoR1) {

		return this.get(fechaInicioR1, fechaTerminoR1, null, null);
	}

	@Override
	public RespuestaDTO filtrar(RespuestaDTO respuestaDTO, String pais) throws CloneNotSupportedException {
		
		List<FeatureDTO> filtrados=respuestaDTO.getFeatures().stream()
		  .filter(f -> f.getProperties().getPlace().endsWith(pais))
		  .collect(Collectors.toList());
		RespuestaDTO copia=(RespuestaDTO) respuestaDTO.clone();
		copia.setFeatures(filtrados);
		
		copia.getMetadata().setCount(respuestaDTO.getFeatures().size());
		return copia;
	}

	@Override
	public Integer count(LocalDate startTime, LocalDate endTime) {
		return this.count(startTime, endTime, null, null);
	}

	
}
