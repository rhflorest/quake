package com.quake.service;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.quake.dto.RespuestaDTO;
/***
 * 
 * @author Rodrigo Flores
 *
 */
@Service
public interface EarthQuakeService {
	
	/***
	 * obtiene lista de eventos en un rango de fechas y magnitudes
	 * @param startTime
	 * @param endtime
	 * @param minMagnitude
	 * @param maxMagnitude
	 * @return eventos qeu cumplen con los parametros
	 */
	public RespuestaDTO get(LocalDate startTime, LocalDate endtime,Double minMagnitude,Double maxMagnitude);
	/***
	 * cuenta los evento en un rango de fechas y magnitudes
	 * @param startTime
	 * @param endtime
	 * @param minMagnitude
	 * @param maxMagnitude
	 * @return
	 */
	public Integer count(LocalDate startTime, LocalDate endtime,Double minMagnitude,Double maxMagnitude);
	/***
	 * otiene los eventos en un rango de fechas
	 * @param fechaInicioR1
	 * @param fechaTerminoR1
	 * @return
	 */
	public RespuestaDTO get(LocalDate fechaInicioR1, LocalDate fechaTerminoR1);
	/***
	 * Filtra una lista de resultados dejando solo lo que cumplen con la condicion del pais
	 * @param respuestaDTO lista antes de filtrar
	 * @param pais pais a filtrar
	 * @return lista filtrada
	 * @throws CloneNotSupportedException error al clonar objeto
	 */
	public RespuestaDTO filtrar(RespuestaDTO respuestaDTO,String pais) throws CloneNotSupportedException;
	/***
	 *  Cuenta  los eventos en un rango de fechas
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Integer count(LocalDate startTime, LocalDate endTime);

}
