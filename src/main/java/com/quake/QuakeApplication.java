package com.quake;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/***
 * 
 * @author Rodrigo Flores
 *
 */
@SpringBootApplication
public class QuakeApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuakeApplication.class, args);
	}

}
