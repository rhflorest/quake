package com.quake.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quake.dto.RespuestaDTO;
import com.quake.service.EarthQuakeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;

/***
 * Api manejo de eventos sismicos
 * 
 * @author Rodrigo Flores
 * 
 */
@Api(value = "Api manejo de eventos sismicos")
@RestController
public class QuakeController {
	@Autowired
	EarthQuakeService earthQuakeService;

	/***
	 * Obtiene eventos que cumplen con los criterios de b�squeda, todos son
	 * opcionales
	 * 
	 * @param startTime
	 * @param endTime
	 * @param minMagnitude
	 * @param maxMagnitude
	 * @param country
	 * @return
	 * @throws CloneNotSupportedException
	 */
	@ApiOperation(value = "eventos que cumplen con los criterios de b�squeda, todos son opcionales", 
			response = RespuestaDTO[].class,
			 authorizations = @Authorization(value = "Bearer"))
	@RequestMapping(value = "/eventos", method = RequestMethod.GET)
	public ResponseEntity<Object> get(
			@RequestParam(name = "startTime", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate startTime,
			@RequestParam(name = "endTime", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate endTime,
			@RequestParam(name = "minMagnitude", required = false) Double minMagnitude,
			@RequestParam(name = "maxMagnitude", required = false) Double maxMagnitude,
			@RequestParam(name = "country", required = false) String country) throws CloneNotSupportedException {
		RespuestaDTO respuestaDTO = this.earthQuakeService.get(startTime, endTime, minMagnitude, maxMagnitude);
		if (country != null) {
			respuestaDTO = this.earthQuakeService.filtrar(respuestaDTO, country);
		}

		return new ResponseEntity<Object>(respuestaDTO, HttpStatus.OK);
	}

	/***
	 * End point para traer dos rangos de fechas
	 * 
	 * @param fechaInicioR1  inicio de rango 1 en formato yyyy-MM-dd
	 * @param fechaTerminoR1 fin rango 2 en formato yyyy-MM-dd
	 * @param fechaInicioR2  inicio rango 1 en formato yyyy-MM-dd
	 * @param fechaTerminoR2 fin rango 1 en formato yyyy-MM-dd
	 * @return devuelve una lista con dos rangos con todos los eventos que
	 *         cohincidan con las fechas
	 */

	@ApiOperation(value = "End point para traer dos rangos de fechas ",
			response = RespuestaDTO[].class,
			 authorizations = @Authorization(value = "Bearer"))
	@RequestMapping(value = "/rango", 
	        method = RequestMethod.GET)
	public ResponseEntity<Object> gettramos(
			@ApiParam(value = "yyyy-MM-dd", required = true) @RequestParam(name = "fechaInicioR1") @DateTimeFormat(iso = ISO.DATE) LocalDate fechaInicioR1,
			@ApiParam(value = "yyyy-MM-dd", required = true) @RequestParam(name = "fechaTerminoR1") @DateTimeFormat(iso = ISO.DATE) LocalDate fechaTerminoR1,
			@ApiParam(value = "yyyy-MM-dd", required = true) @RequestParam(name = "fechaInicioR2") @DateTimeFormat(iso = ISO.DATE) LocalDate fechaInicioR2,
			@ApiParam(value = "yyyy-MM-dd", required = true) @RequestParam(name = "fechaTerminoR2") @DateTimeFormat(iso = ISO.DATE) LocalDate fechaTerminoR2) {
		RespuestaDTO tramo1 = this.earthQuakeService.get(fechaInicioR1, fechaTerminoR1);
		RespuestaDTO tramo2 = this.earthQuakeService.get(fechaInicioR2, fechaTerminoR2);

		List<RespuestaDTO> tramos = new ArrayList<RespuestaDTO>();
		tramos.add(tramo1);
		tramos.add(tramo2);
		return new ResponseEntity<Object>(tramos, HttpStatus.OK);
	}

	/***
	 * End point para contar seg�n criterios de b�squeda
	 * 
	 * @param startTime Fecha inicial
	 * @param endTime   Fecha final
	 * @param country1  Primer pais
	 * @param country2  segundo pais
	 * @return regresa cantidad contada
	 * @throws CloneNotSupportedException exceci�n en caso de que la respuesta del
	 *                                    servicio no sea clonable
	 */
	@ApiOperation(value = "contar seg�n criterios de b�squeda", response = Integer.class,
			 authorizations = @Authorization(value = "Bearer"))
	@RequestMapping(value = "/contar", method = RequestMethod.GET)
	public ResponseEntity<Object> contar(
			@ApiParam(value = "yyyy-MM-dd", required = false) @RequestParam(name = "startTime", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate startTime,
			@ApiParam(value = "yyyy-MM-dd", required = false) @RequestParam(name = "endTime", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate endTime,
			@RequestParam(name = "country1", required = false) String country1,
			@RequestParam(name = "country2", required = false) String country2) throws CloneNotSupportedException {
		Integer contar = null;
		if (country1 == null) {
			contar = this.earthQuakeService.count(startTime, endTime);
		} else {
			RespuestaDTO respuestaDTO = this.earthQuakeService.get(startTime, endTime);
			RespuestaDTO filtrada = this.earthQuakeService.filtrar(respuestaDTO, country1);
			contar = filtrada.getMetadata().getCount();
			RespuestaDTO filtrada2 = this.earthQuakeService.filtrar(respuestaDTO, country2);
			contar = contar + filtrada2.getMetadata().getCount();
		}
		return new ResponseEntity<Object>(contar, HttpStatus.OK);
	}

}
