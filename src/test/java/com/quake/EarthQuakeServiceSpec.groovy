package com.quake

import com.quake.dto.RespuestaDTO
import com.quake.service.EarthQuakeService

import net.bytebuddy.asm.Advice.This

import java.time.LocalDate
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
/***
 * 
 * @author Rodrigo Flores
 *
 */
@SpringBootTest
class EarthQuakeServiceSpec  extends Specification {

	@Autowired
	EarthQuakeService earthQuakeService;

	@Test
	def "service get object"() {
		expect:
		LocalDate inicio=LocalDate.now().plusDays(-1);
		LocalDate fin=LocalDate.now();
		this.earthQuakeService.get(inicio,fin,6.5,7).getType().equals("FeatureCollection");
	}
	@Test
	def "service get count more zero"() {
		expect:
		LocalDate inicio=LocalDate.now().plusDays(-1);
		LocalDate fin=LocalDate.now();
		this.earthQuakeService.count(inicio,fin,6.5,7)>=0
	}
	@Test
	def "service filter result"() {
		expect:
		LocalDate inicio=LocalDate.now().plusDays(-1);
		LocalDate fin=LocalDate.now();
		RespuestaDTO respuesta=this.earthQuakeService.get(inicio,fin,6.5,7);
		this.getEarthQuakeService().filtrar(respuesta, "CA")
	}
}
